# ReWord Web

## Installation

1. Install dependencies with `npm i`.
2. Fill out Firebase config in _src/environments/firebase.example.ts_ and rename it to _firebase.ts_.
3. Fill out api config in _src/environments/api.example.ts_ and rename it to _api.ts_.

## Development server

Run `ng serve` for a dev server. Navigate to _http://localhost:4200/_. The app will automatically reload if you change any of the source files.

## Build for deployment

Run `ng build --prod` to build the project for deployment. The build artifacts will be stored in the _dist/reword-web_ directory.

## Further help

This project was generated with [Angular CLI](https://github.com/angular/angular-cli). To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
