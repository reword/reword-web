import {firebase} from './firebase';
import {api} from './api';

export const environment = {
  production: true,
  gameTable: 'Game',
  userTable: 'User',
  baseUrl: api.baseUrl,
  firebase
};
