import {Component, OnInit} from '@angular/core';
import {GameService} from '../game.service';
import {Observable} from 'rxjs';
import {Game} from '../model/game';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {CreateGameModalComponent} from '../create-game-modal/create-game-modal.component';
import {FireUser} from '../model/fire-user';
import {UserService} from '../user.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  publicGames$: Observable<Game[]>;
  fireUser: FireUser;

  constructor(private gameService: GameService, private userService: UserService, private modalService: NgbModal) {
  }

  ngOnInit(): void {
    this.publicGames$ = this.gameService.getPublicGames();
    this.userService.getCurrentFireUser().subscribe(fireUser => this.fireUser = fireUser);
  }

  openCreateGameModal() {
    const modalRef = this.modalService.open(CreateGameModalComponent);
    modalRef.componentInstance.fireUserId = this.fireUser?.id;
  }
}
