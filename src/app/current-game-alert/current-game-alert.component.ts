import {Component, Input, OnInit} from '@angular/core';
import {GameService} from '../game.service';

@Component({
  selector: 'app-current-game-alert',
  templateUrl: './current-game-alert.component.html',
  styleUrls: ['./current-game-alert.component.css']
})
export class CurrentGameAlertComponent implements OnInit {

  @Input() fireUserId: string;
  @Input() UserCurrentGameId: string;
  gameName: string;

  constructor(private gameService: GameService) {
  }

  ngOnInit(): void {
    this.gameName = this.UserCurrentGameId.split('_')[0];
  }

  exitGame() {
    this.gameService.exitGame(this.fireUserId).subscribe(
      () => console.log('success'),
      err => console.error(err)
    );
  }
}
