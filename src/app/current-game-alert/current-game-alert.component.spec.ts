import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CurrentGameAlertComponent} from './current-game-alert.component';

describe('CurrentGameAlertComponent', () => {
  let component: CurrentGameAlertComponent;
  let fixture: ComponentFixture<CurrentGameAlertComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CurrentGameAlertComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CurrentGameAlertComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
