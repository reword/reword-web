import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WritersWriteAnswersComponent } from './writers-write-answers.component';

describe('WritersWriteAnswersComponent', () => {
  let component: WritersWriteAnswersComponent;
  let fixture: ComponentFixture<WritersWriteAnswersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WritersWriteAnswersComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WritersWriteAnswersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
