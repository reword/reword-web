import {Component, Input, OnInit} from '@angular/core';
import {Game} from '../../model/game';
import {FireUser} from '../../model/fire-user';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {GameService} from '../../game.service';

@Component({
  selector: 'app-writers-write-answers',
  templateUrl: './writers-write-answers.component.html',
  styleUrls: ['./writers-write-answers.component.css']
})
export class WritersWriteAnswersComponent implements OnInit {

  @Input() game: Game;
  @Input() fireUser: FireUser;
  @Input() currentUserIndex: number;
  yourTurn;
  writeTextForm = new FormGroup({
    completion: new FormControl('', [Validators.required, Validators.minLength(10)])
  });
  error;

  constructor(private gameService: GameService) { }

  ngOnInit(): void {
    this.yourTurn = this.game.variables.CONFIRMED_CURRENT_HAND[this.currentUserIndex] === false &&
      this.game.variables.READER_INDEX !== this.currentUserIndex;
  }

  writeText() {
    const data = this.writeTextForm.value;
    const fireUserId = this.fireUser.id;
    this.gameService.userReply(fireUserId, data.completion).subscribe(
      () => this.gameService.userReply(fireUserId, '✅ YES').subscribe(
        () => this.yourTurn = false
      )
    );
  }

}
