import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {GameDetailInitialComponent} from './game-detail-initial.component';

describe('GameDetailInitialComponent', () => {
  let component: GameDetailInitialComponent;
  let fixture: ComponentFixture<GameDetailInitialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GameDetailInitialComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GameDetailInitialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
