import {Component, Input, OnInit} from '@angular/core';
import {Game} from '../../model/game';
import {FireUser} from '../../model/fire-user';
import {GameService} from '../../game.service';

@Component({
  selector: 'app-game-detail-initial',
  templateUrl: './game-detail-initial.component.html',
  styleUrls: ['./game-detail-initial.component.css']
})
export class GameDetailInitialComponent implements OnInit {

  @Input() game: Game;
  @Input() fireUser: FireUser;
  @Input() playMode: boolean;
  currentUrl: string;

  constructor(private gameService: GameService) {
  }

  ngOnInit(): void {
    this.currentUrl = window.location.href;
  }

  startGame() {
    this.gameService.userReply(this.fireUser.id, '🏁 Start').subscribe(
      () => console.log('game started'),
      err => console.error(err)
    );
  }
}
