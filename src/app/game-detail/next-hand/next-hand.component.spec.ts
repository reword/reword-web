import {ComponentFixture, TestBed} from '@angular/core/testing';

import {NextHandComponent} from './next-hand.component';

describe('NextHandComponent', () => {
  let component: NextHandComponent;
  let fixture: ComponentFixture<NextHandComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [NextHandComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NextHandComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
