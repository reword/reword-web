import {Component, Input, OnInit} from '@angular/core';
import {Game} from '../../model/game';
import {FireUser} from '../../model/fire-user';
import {AnswerInfo} from '../../model/answer-info';
import {GameService} from '../../game.service';

class Score {
  num: number;
  points: number;
}

@Component({
  selector: 'app-next-hand',
  templateUrl: './next-hand.component.html',
  styleUrls: ['./next-hand.component.css']
})
export class NextHandComponent implements OnInit {

  @Input() game: Game;
  @Input() fireUser: FireUser;
  @Input() shuffledAnswerInfos: AnswerInfo[];
  @Input() currentUserIndex: number;
  @Input() hands: number[];
  scores: Score[];
  yourTurn: boolean;
  isLoadingStartNextRound = false;
  isGameEnded = false;

  constructor(private gameService: GameService) {
  }

  ngOnInit(): void {
    const scores: Score[] = [];
    for (const [num, points] of this.game.variables.GAME_POINTS.entries()) {
      scores.push({num, points});
    }
    scores.sort((a, b) => b.points - a.points);
    this.scores = scores;
    this.yourTurn = this.currentUserIndex === 0;

    this.isGameEnded = this.game.variables.HAND === this.game.num_hands;
  }

  startNextRound() {
    this.isLoadingStartNextRound = true;
    this.gameService.userReply(this.fireUser?.id, '🚦 NEXT ROUND').subscribe(() => this.isLoadingStartNextRound = false);
  }

}
