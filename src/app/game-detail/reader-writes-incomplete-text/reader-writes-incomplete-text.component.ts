import {Component, Input, OnInit} from '@angular/core';
import {Game} from '../../model/game';
import {FireUser} from '../../model/fire-user';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {GameService} from '../../game.service';

@Component({
  selector: 'app-reader-writes-incomplete-text',
  templateUrl: './reader-writes-incomplete-text.component.html',
  styleUrls: ['./reader-writes-incomplete-text.component.css']
})
export class ReaderWritesIncompleteTextComponent implements OnInit {

  @Input() game: Game;
  @Input() fireUser: FireUser;
  @Input() currentUserIndex: number;
  yourTurn;
  writeTextForm = new FormGroup({
    incomplete: new FormControl('', [Validators.required, Validators.minLength(10)]),
    completion: new FormControl('', [Validators.required, Validators.minLength(10)])
  });
  error;
  isLoadingWriteText = false;

  constructor(private gameService: GameService) { }

  ngOnInit(): void {
    this.yourTurn = this.game.variables.READER_INDEX === this.currentUserIndex;
  }

  writeText() {
    this.isLoadingWriteText = true;
    const data = this.writeTextForm.value;
    const fireUserId = this.fireUser.id;
    this.gameService.userReply(fireUserId, data.incomplete).subscribe(
      () => this.gameService.userReply(fireUserId, data.completion).subscribe(
        () => this.gameService.userReply(fireUserId, '✅ YES').subscribe(
          () => this.isLoadingWriteText = false
        )
      )
    );
  }
}
