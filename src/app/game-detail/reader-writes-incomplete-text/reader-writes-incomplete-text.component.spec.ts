import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReaderWritesIncompleteTextComponent } from './reader-writes-incomplete-text.component';

describe('ReaderWritesIncompleteTextComponent', () => {
  let component: ReaderWritesIncompleteTextComponent;
  let fixture: ComponentFixture<ReaderWritesIncompleteTextComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReaderWritesIncompleteTextComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReaderWritesIncompleteTextComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
