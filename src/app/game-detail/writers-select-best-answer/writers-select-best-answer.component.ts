import {Component, Input, OnInit} from '@angular/core';
import {Game} from '../../model/game';
import {FireUser} from '../../model/fire-user';
import {AnswerInfo} from '../../model/answer-info';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {GameService} from '../../game.service';

@Component({
  selector: 'app-writers-select-best-answer',
  templateUrl: './writers-select-best-answer.component.html',
  styleUrls: ['./writers-select-best-answer.component.css']
})
export class WritersSelectBestAnswerComponent implements OnInit {

  @Input() game: Game;
  @Input() fireUser: FireUser;
  @Input() currentUserIndex: number;
  @Input() shuffledAnswerInfos: AnswerInfo[];
  yourTurn: boolean;
  selectAnswerForm = new FormGroup({
    answer: new FormControl('', [Validators.required])
  });
  keyboard: string[];
  isLoadingSelectAnswer = false;

  constructor(private gameService: GameService) { }

  ngOnInit(): void {
    this.yourTurn = this.game.variables.SELECTED_CURRENT_HAND[this.currentUserIndex] === false
      && this.game.variables.READER_INDEX !== this.currentUserIndex;
    if (this.yourTurn) {
      this.keyboard = this.fireUser.keyboard[0];
    }
  }

  selectAnswer(answer: string) {
    this.isLoadingSelectAnswer = true;
    this.gameService.userReply(this.fireUser.id, answer).subscribe(() => {
      this.yourTurn = false;
      this.isLoadingSelectAnswer = false;
    });
  }
}
