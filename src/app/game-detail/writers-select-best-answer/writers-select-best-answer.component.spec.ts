import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WritersSelectBestAnswerComponent } from './writers-select-best-answer.component';

describe('WritersSelectBestAnswerComponent', () => {
  let component: WritersSelectBestAnswerComponent;
  let fixture: ComponentFixture<WritersSelectBestAnswerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WritersSelectBestAnswerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WritersSelectBestAnswerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
