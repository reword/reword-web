import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Game} from '../model/game';
import {GameService} from '../game.service';
import {AnswerInfo} from '../model/answer-info';
import {NgbModal, NgbModalConfig, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {UserService} from '../user.service';
import {FireUser} from '../model/fire-user';
import {JoinGameModalComponent} from '../join-game-modal/join-game-modal.component';

@Component({
  selector: 'app-game-detail',
  templateUrl: './game-detail.component.html',
  styleUrls: ['./game-detail.component.css']
})
export class GameDetailComponent implements OnInit {
  game: Game;
  shuffledAnswerInfos: AnswerInfo[];
  hands: number[];
  playMode = false;
  fireUser: FireUser;
  isModelOpen = false;
  modalRef: NgbModalRef;
  currentUserIndex: number;

  constructor(private route: ActivatedRoute, private gameService: GameService, private modalService: NgbModal,
              config: NgbModalConfig, private userService: UserService, private router: Router) {
    config.backdrop = 'static';
    config.keyboard = false;
  }

  ngOnInit(): void {
    this.route.paramMap.subscribe(paramMap => {
      const id = paramMap.get('id');
      this.userService.getCurrentFireUser().subscribe(fireUser => {
        this.fireUser = fireUser;
        this.gameService.getGame(id).subscribe(game => {
          if (!game) {
            this.game = null;
            return;
          }

          if (fireUser) {
            this.currentUserIndex = game.players_id.indexOf(fireUser.id);
            game.players_names[this.currentUserIndex] = 'Tu';
          }
          this.game = game;
          this.hands = this.gameService.getHandsArray(game);
          if (game.sub_state === 'state_WRITERS_SELECT_BEST_ANSWER' || game.sub_state === 'state_NEXT_HAND' || game.sub_state === 'state_RECAP_VOTES') {
            this.shuffledAnswerInfos = this.gameService.getShuffledAnswerInfos(game);
          }

          // If user is not authenticated or is not a player in any other game, then open modal
          if (!fireUser || !fireUser?.current_game_id) {
            this.openJoinGameModal();
            return;
          }

          // If user is a player in this game, then continue playing
          if (fireUser.current_game_id === game?.id) {
            this.playMode = true;
            return;
          }
        });
      });
    });
  }

  openJoinGameModal() {
    if (this.isModelOpen) {
      return;
    }
    this.modalRef = this.modalService.open(JoinGameModalComponent);
    this.modalRef.componentInstance.getPlayMode = this.playMode;
    this.modalRef.componentInstance.game = this.game;
    this.modalRef.componentInstance.fireUserId = this.fireUser?.id;
    this.modalRef.componentInstance.getPlayModeChange.subscribe(playMode => {
      this.playMode = playMode;
    });
    this.isModelOpen = true;
  }

  exitGame() {
    this.gameService.exitGame(this.fireUser.id).subscribe(
      () => {
        this.modalRef.close();
        this.router.navigate(['/']);
      },
      err => console.error(err)
    );
  }
}
