import {Injectable} from '@angular/core';
import {User} from 'firebase';
import {AngularFireAuth} from '@angular/fire/auth';
import {AngularFirestore} from '@angular/fire/firestore';
import {environment} from '../environments/environment';
import {HttpClient} from '@angular/common/http';
import {FireUser} from './model/fire-user';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private afAuth: AngularFireAuth, private afs: AngularFirestore, private http: HttpClient) {
  }

  signOut() {
    return this.afAuth.signOut();
  }

  loginWithEmail(email, password) {
    return this.afAuth.signInWithEmailAndPassword(email, password);
  }

  registerWithEmail(displayName, email, password): Promise<User> {
    return new Promise((resolve, error) => {
      this.afAuth.createUserWithEmailAndPassword(email, password).then(userCredential => {
        const user = userCredential.user;
        this.setDisplayName(user, displayName).then(() => {
          this.addUserToFirestore(user, displayName).subscribe(
            () => resolve(user),
            err => error(err)
          );
        });
      }).catch(err => error(err));
    });
  }

  addUserToFirestore(user: User, displayName: string) {
    const formData: FormData = new FormData();
    formData.append('id', 'web_' + user.uid);
    formData.append('name', displayName);
    return this.http.post(environment.baseUrl + 'add_user', formData);
  }

  setDisplayName(user: User, displayName) {
    return user.updateProfile({displayName});
  }

  resetPassword(email) {
    return this.afAuth.sendPasswordResetEmail(email);
  }

  getFireUser(user: User) {
    return this.afs.collection(environment.userTable).doc<FireUser>('web_' + user.uid).valueChanges();
  }

  getCurrentFireUser(): Observable<FireUser> {
    return new Observable<FireUser>(subscriber => {
      this.afAuth.user.subscribe(user => {
        if (user) {
          this.afs.collection(environment.userTable).doc<FireUser>('web_' + user.uid).valueChanges()
            .subscribe(fireUser => {
              subscriber.next(fireUser);
            });
        } else {
          subscriber.next(undefined);
        }
      });
    });
  }
}
