import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {GameDetailComponent} from './game-detail/game-detail.component';
import {HomeComponent} from './home/home.component';


const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'game/:id', component: GameDetailComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
