import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {AngularFireModule} from '@angular/fire';
import {environment} from '../environments/environment';
import {AngularFirestoreModule} from '@angular/fire/firestore';
import {GameDetailComponent} from './game-detail/game-detail.component';
import {HomeComponent} from './home/home.component';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {NgbAccordionModule, NgbCollapseModule, NgbDropdownModule} from '@ng-bootstrap/ng-bootstrap';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {SignInComponent} from './sign-in/sign-in.component';
import {JoinGameModalComponent} from './join-game-modal/join-game-modal.component';
import {CurrentGameAlertComponent} from './current-game-alert/current-game-alert.component';
import {GameDetailInitialComponent} from './game-detail/game-detail-initial/game-detail-initial.component';
import {CreateGameModalComponent} from './create-game-modal/create-game-modal.component';
import {ReaderWritesIncompleteTextComponent} from './game-detail/reader-writes-incomplete-text/reader-writes-incomplete-text.component';
import {WritersWriteAnswersComponent} from './game-detail/writers-write-answers/writers-write-answers.component';
import {WritersSelectBestAnswerComponent} from './game-detail/writers-select-best-answer/writers-select-best-answer.component';
import {NextHandComponent} from './game-detail/next-hand/next-hand.component';

@NgModule({
  declarations: [
    AppComponent,
    GameDetailComponent,
    HomeComponent,
    SignInComponent,
    JoinGameModalComponent,
    CurrentGameAlertComponent,
    GameDetailInitialComponent,
    CreateGameModalComponent,
    ReaderWritesIncompleteTextComponent,
    WritersWriteAnswersComponent,
    WritersSelectBestAnswerComponent,
    NextHandComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    FontAwesomeModule,
    NgbAccordionModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgbDropdownModule,
    NgbCollapseModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
