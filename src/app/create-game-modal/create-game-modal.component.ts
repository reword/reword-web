import {Component, Input, OnInit} from '@angular/core';
import {GameService} from '../game.service';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {ApiResponse} from '../model/api-response';
import {HttpErrorResponse} from '@angular/common/http';

@Component({
  selector: 'app-create-game-modal',
  templateUrl: './create-game-modal.component.html',
  styleUrls: ['./create-game-modal.component.css']
})
export class CreateGameModalComponent implements OnInit {
  @Input() fireUserId: string;
  createGameForm = new FormGroup({
    gameName: new FormControl('', [Validators.required, Validators.pattern('[A-Za-z0-9\-\_]+$')])
  });
  error;

  constructor(private gameService: GameService, public activeModal: NgbActiveModal, private router: Router) {
  }

  ngOnInit(): void {
  }

  createGame() {
    const gameName = this.createGameForm.value.gameName;
    this.gameService.createGame(this.fireUserId, gameName).subscribe(
      (response: ApiResponse) => {
        this.close();
        this.router.navigate(['/game/', response.data]);
      },
      (err: HttpErrorResponse) => {
        const response: ApiResponse = err.error;
        switch (response.error) {
          case 'user not in state_INITIAL':
            this.error = 'Partecipi già a un gioco. Esci prima da quello per poterne creare un nuovo.';
            break;
          case 'There is already an active game with the same name':
            this.error = 'C\'è già un gioco attivo con lo stesso nome. Scegli un nome diverso.';
            break;
          default:
            this.error = 'C\'è stato un errore in fase di creazione del gioco.';
        }
      }
    );
  }

  close() {
    this.activeModal.close();
  }
}
