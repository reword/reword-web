export interface HandPoints {
  [player: number]: number;
}
