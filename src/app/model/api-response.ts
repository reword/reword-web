export interface ApiResponse {
  success: boolean;
  error?: string;
  data?: string;
}
