export interface AnswerInfo {
  answer: string;
  authors: number[];
  correct: boolean;
  shuffled_number: number;
  voted_by: number[];
}
