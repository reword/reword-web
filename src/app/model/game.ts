import {Variables} from './variables';
import * as firebase from 'firebase';

export interface Game {
  announced: boolean;
  created: firebase.firestore.Timestamp;
  creator_id: string;
  game_control: string;
  game_type: string;
  id: string;
  language: string;
  modified: firebase.firestore.Timestamp;
  name: string;
  num_hands: number;
  num_players: number;
  players_id: string[];
  players_names: string[];
  state: string;
  sub_state: string;
  translate_help: boolean;
  variables: Variables;
}
