import * as firebase from 'firebase';

export interface FireUser {
  application: string;
  bot: boolean;
  created: firebase.firestore.Timestamp;
  current_game_id: string;
  id: string;
  keyboard?: any;
  language?: any;
  modified: firebase.firestore.Timestamp;
  name: string;
  notifications: boolean;
  serial_id: string;
  state: string;
  username?: any;
}
