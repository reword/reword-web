import {HandPoints} from './hand-points';
import {AnswerInfoHand} from './answer-info-hand';

export interface Variables {
  ALL_NUM_COMPLETED_ANSWERS: string;
  ANSWERS_INFO: AnswerInfoHand[];
  CONFIRMED_CURRENT_HAND: boolean[];
  GAME_POINTS: number[];
  HAND: number;
  HAND_POINTS: HandPoints[];
  INCOMPLETE_TEXTS: string[];
  ORIGINAL_COMPLETION: string[];
  READER_INDEX: number;
  SELECTED_CURRENT_HAND: boolean[];
  WINNERS_NAMES: string[];
}
