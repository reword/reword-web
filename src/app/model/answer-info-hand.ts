import {AnswerInfo} from './answer-info';

export interface AnswerInfoHand {
  [continuation: string]: AnswerInfo;
}
