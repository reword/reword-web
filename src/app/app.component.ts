import {Component, OnInit} from '@angular/core';
import {UserService} from './user.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  fireUserId: string;
  isMenuCollapsed = true;

  constructor(private userService: UserService, public modalService: NgbModal) {
  }

  ngOnInit(): void {
    this.userService.getCurrentFireUser().subscribe(fireUser => this.fireUserId = fireUser?.id);
  }

  signOut() {
    this.userService.signOut();
  }
}
