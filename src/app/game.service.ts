import {Injectable} from '@angular/core';
import {Game} from './model/game';
import {AngularFirestore} from '@angular/fire/firestore';
import {Observable} from 'rxjs';
import {AnswerInfo} from './model/answer-info';
import {environment} from '../environments/environment';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class GameService {

  constructor(private afs: AngularFirestore, private http: HttpClient) {
  }

  getGame(id: string): Observable<Game> {
    return this.afs.collection(environment.gameTable).doc<Game>(id).valueChanges();
  }

  getShuffledAnswerInfos(game: Game): AnswerInfo[] {
    const answerInfoHand = game.variables.ANSWERS_INFO[game.variables.HAND - 1];

    const answerInfos = Object.values(answerInfoHand);

    answerInfos.sort((a, b) => {
      return a.shuffled_number - b.shuffled_number;
    });

    return answerInfos;
  }

  getHandsArray(game: Game): number[] {
    return [...Array(game.variables.HAND).keys()];
  }

  getPublicGames(): Observable<Game[]> {
    return this.afs
      .collection<Game>(environment.gameTable,
        ref => ref.where('announced', '==', true).where('state', '==', 'INITIAL'))
      .valueChanges();
  }

  registerPlayer(userId: string, gameName: string) {
    const formData: FormData = new FormData();
    formData.append('id', userId);
    formData.append('game_name', gameName);
    return this.http.post(environment.baseUrl + 'join_game', formData);
  }

  userReply(fireUserId: string, reply: string) {
    const formData: FormData = new FormData();
    formData.append('id', fireUserId);
    formData.append('reply', reply);
    return this.http.post(environment.baseUrl + 'user_reply', formData);
  }

  createGame(userId: string, gameName: string) {
    const formData: FormData = new FormData();
    formData.append('id', userId);
    formData.append('game_name', gameName);
    return this.http.post(environment.baseUrl + 'create_game', formData);
  }

  exitGame(fireUserId: string) {
    return this.userReply(fireUserId, '/exit');
  }
}
