import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {UserService} from '../user.service';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})
export class SignInComponent implements OnInit {

  @Output() getFireUserId = new EventEmitter<string>();
  loginForm = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', Validators.required),
  });
  registerForm = new FormGroup({
    displayName: new FormControl('', Validators.required),
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', [Validators.required, Validators.minLength(6)]),
  });
  loginError;
  registerError;
  loginSuccess;
  redirectRoute;

  constructor(private userService: UserService) {
  }

  ngOnInit(): void {
  }

  loginWithEmail() {
    const data = this.loginForm.value;

    this.userService.loginWithEmail(data.email, data.password)
      .then(userCredential => this.getFireUserId.emit('web_' + userCredential.user.uid))
      .catch(err => {
        switch (err.code) {
          case 'auth/invalid-email':
            this.loginError = 'Formato email non valido';
            break;
          case 'auth/user-disabled':
            this.loginError = 'Utente disabilitato.';
            break;
          case 'auth/user-not-found':
            this.loginError = 'Non esiste alcun utente con quella email.';
            break;
          case 'auth/wrong-password':
            this.loginError = 'Password errata.';
            break;
        }
      });
  }

  registerWithEmail() {
    const data = this.registerForm.value;

    this.userService.registerWithEmail(data.displayName, data.email, data.password)
      .then(user => this.getFireUserId.emit('web_' + user.uid))
      .catch(err => {
        switch (err.code) {
          case 'auth/email-already-in-use':
            this.registerError = 'Questo indirizzo email è già in uso.';
            break;
          case 'auth/invalid-email':
            this.registerError = 'Formato indirizzo email non valido.';
            break;
          case 'auth/operation-not-allowed':
            this.registerError = 'L\'autenticazione tramite email/password non è attiva al momento.';
            break;
          case 'auth/weak-password':
            this.registerError = 'La password è troppo debole.';
            break;
        }
      });

  }

  resetPassword() {
    const email = this.loginForm.value.email;

    if (!email) {
      this.loginError = 'Inserisci l\'indirizzo email nel campo email e riclicca \'password dimenticata?\'.';
      return;
    }

    this.userService.resetPassword(email).then(() => {
      this.loginSuccess = 'Ti abbiamo inviato un\'email con il link per reimpostare la password. ' +
        'Se non la trovi, controlla anche la cartella spam.';
    }).catch(err => {
      switch (err.code) {
        case 'auth/invalid-email':
          this.loginError = 'Formato email non valido';
          break;
        case 'auth/user-not-found':
          this.loginError = 'Non esiste alcun utente con quella email.';
          break;
      }
    });
  }
}
