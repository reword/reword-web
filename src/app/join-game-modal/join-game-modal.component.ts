import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {GameService} from '../game.service';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {UserService} from '../user.service';
import {Game} from '../model/game';
import {HttpErrorResponse} from '@angular/common/http';
import {ApiResponse} from '../model/api-response';

@Component({
  selector: 'app-join-game-modal',
  templateUrl: './join-game-modal.component.html',
  styleUrls: ['./join-game-modal.component.css']
})
export class JoinGameModalComponent implements OnInit {
  @Input() getPlayMode: boolean;
  @Input() game: Game;
  @Input() fireUserId: string;
  @Output() getPlayModeChange = new EventEmitter<boolean>();
  error;

  constructor(private userService: UserService, public activeModal: NgbActiveModal, private gameService: GameService) {
  }

  ngOnInit(): void {
  }

  setPlayMode(playMode: boolean) {
    this.getPlayMode = playMode;
    this.getPlayModeChange.emit(playMode);
  }

  joinGame() {
    this.gameService.registerPlayer(this.fireUserId, this.game.name).subscribe(
      () => this.close(true),
      (err: HttpErrorResponse) => {
        const response: ApiResponse = err.error;
        switch (response.error) {
          case 'user not in state_INITIAL':
            this.error = 'Partecipi già a un gioco. Esci prima da quello per poter partecipare a un altro.';
            break;
          case 'user does not exist':
            this.error = 'L\'utente non esiste.';
            break;
          case 'Game no longer available':
            this.error = 'Il gioco non è più disponibile.';
            break;
          case 'Game does not exist':
            this.error = 'Il gioco non esiste.';
            break;
          default:
            console.error(response.error);
            this.error = 'C\'è stato un errore in fase di partecipazione al gioco.';
        }
      }
    );
  }

  close(playMode) {
    this.setPlayMode(playMode);
    this.activeModal.close();
  }
}
