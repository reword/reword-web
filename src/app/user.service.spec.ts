import {TestBed} from '@angular/core/testing';

import {UserService} from './user.service';
import {BehaviorSubject} from 'rxjs';
import {AngularFireAuth} from '@angular/fire/auth';

describe('UserService', () => {
  let service: UserService;

  const authStub: any = {
    authState: {},
    auth: () => new BehaviorSubject({
      displayName: 'Mario Rossi'
    })
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        {provide: AngularFireAuth, useValue: authStub}
      ]
    });
    service = TestBed.inject(UserService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
