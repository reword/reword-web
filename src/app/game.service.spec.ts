import {TestBed} from '@angular/core/testing';

import {GameService} from './game.service';
import {AngularFirestore} from '@angular/fire/firestore';
import {BehaviorSubject} from 'rxjs';

export const FirestoreStub = {
  collection: () => ({
    doc: () => ({
      valueChanges: () => new BehaviorSubject({
        game_type: 'CONTINUATION',
        id: 'web_copy_04',
        num_players: 3,
        announced: false,
        game_control: 'DEFAULT',
        translate_help: false,
        players_id: ['telegram_123', 'telegram_456', 'telegram_789'],
        variables: {
          READER_INDEX: 0,
          ORIGINAL_COMPLETION: ['', '', ''],
          PLAYERS_ANSWERS: [{}, {}, {}],
          SELECTED_CURRENT_HAND: [false, false, false],
          HAND: 1,
          COMPLETED_HANDS: [false, false, true],
          HAND_POINTS: [{0: 0, 1: 0, 2: 0}, {0: 0, 1: 0, 2: 0}, {0: 0, 1: 0, 2: 0}],
          GAME_POINTS: [],
          INCOMPLETE_TEXTS: ['', '', ''],
          WINNERS_NAMES: [],
          CONFIRMED_CURRENT_HAND: [false, false, false],
          ANSWERS_INFO: [{}, {}, {}]
        },
        num_hands: 3,
        modified: 1587575310,
        language: 'it',
        sub_state: 'state_READER_WRITES_INCOMPLETE_TEXT',
        created: 1587575310,
        players_names: ['Tizio', 'Caio', 'Sempronio'],
        creator_id: 'telegram_123',
        state: 'STARTED',
        name: 'WEB'
      }),
      set: () => new Promise((resolve) => resolve()),
    }),
  }),
};

describe('GameService', () => {
  let service: GameService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        {provide: AngularFirestore, useValue: FirestoreStub},
      ]
    });
    service = TestBed.inject(GameService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
